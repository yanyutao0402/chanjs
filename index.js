const express = require("express");
const path = require("path");
const fs = require("fs");
const cors = require("cors");
const config = require("./core/config.js");
const {bindClass,createKnex,loadWebToEnd} = require("./core/helper.js");
const Controller = require("./core/lib/controller.js");
const Service = require("./core/lib/service.js");
const cache = require('./core/lib/cache.js');
const core = require("./core/lib/index.js");
const extend = require("./core/lib/extend.js");
/**
 * @description 基于express封装的mvc框架，遵循约定优于配置原则
 */
class Chan {
  static helper = {};
  static modules = {};
  static plugins = {};
  static config = config;
  static cache = cache;
  static Controller = Controller;
 
  constructor() {
    this.init();
  }

  init() {
    const startTime = performance.now();
    this.app = express();
    extend(this.app);
    this.router = express.Router();
    this.loadConfig();
    this.loadExtends();
    this.loadCore();
    this.loadKnex();
    this.loadCors();
    // 记录结束时间
    const endTime = performance.now();
    console.log(`Chanjs init: ${endTime - startTime} ms`);
  }



  loadConfig() {
    const configPath = path.join(Chan.config.APP_PATH, "config/index.js");
    if (fs.existsSync(configPath)) {
      const config = require(configPath);
      Chan.config = { ...Chan.config, ...config };
    }
  }

  loadExtends() {
    const extendPath = path.join(Chan.config.APP_PATH, "extend");
    if (fs.existsSync(extendPath)) {
      let controllers = fs
        .readdirSync(extendPath)
        .filter((file) => file.endsWith(".js"));
      for (let i = 0, file; i < controllers.length; i++) {
        file = controllers[i];
        const helper = require(path.join(extendPath, file));
        const fileName = file.replace(".js", "");
        Chan.helper[fileName] = helper;
      }
    }
  }

  /**
   * @description app核心模块：日志、favicon 图标、cookie、json、url、模板引擎、静态资源
   */
  loadCore() {
    core(this.app, Chan.config);
  }

  //数据库操作
  loadKnex() {
    if(Chan.config?.db?.length > 0){
      Chan.config.db.map((item,index) => {
        if(index ==0){
          Chan.knex = createKnex(item);
        }else{
          Chan[`knex${index}`] = createKnex(item);
        }
      })
      Chan.Service = Service;
    }
  }

  //开始启动
  beforeStart(cb) {
    cb && cb();
  }
  //启动
  start(cb) {
    const startTime = performance.now();
    this.loadModules();
    this.loadPlugins();
    this.loadCommonRouter();
     // 记录结束时间
    const endTime = performance.now();
    console.log(`Chanjs load modules: ${endTime - startTime} ms`);
    cb && cb();
  }

  //解决跨域
  loadCors() {
    Chan.config?.cors?.origin && this.app.use(cors(Chan.config.cors));
  }

  // 加载插件
  loadPlugins() {
    this.loadModules("plugins");
  }

  /**
   * @description 模块加载入口（路由&控制器& 服务）
   */
  loadModules(modules = "modules") {
    const configPath = path.join(Chan.config.APP_PATH, modules);
    if (fs.existsSync(configPath)) {
      //模块名称
      const dirs = loadWebToEnd(Chan.config[modules]);
      Chan[modules] = {};
      // 先加载所有服务
      dirs.forEach((item) => {
        Chan[modules][item] = {
          service: {},
          controller: {},
        };
        this.loadServices(modules, item);
      });

      // 加载控制器和路由
      dirs.forEach((item) => {
        this.loadModule(modules, item);
      });
    }
  }

  /**
   * @description 加载模块，包括 controller service router
   * @param {String} moduleName 模块名称
   */
  loadModule(modules, moduleName) {
    this.loadControllers(modules, moduleName);
    this.loadRoutes(modules, moduleName);
  }

  loadFiles(modules, moduleName, type) {
    const dir = path.join(Chan.config.APP_PATH, modules, moduleName, type);
    if (fs.existsSync(dir)) {
      const files = fs.readdirSync(dir).filter(file => file.endsWith(".js"));
      files.forEach(file => {
        const module = require(path.join(dir, file));
        const name = file.replace(".js", "");
        Chan[modules][moduleName][type][name] = { ...bindClass(module) };
      });
    }
  }

  /**
   * @description 扫描模块下所有service
   * @param {*} moduleDir 模块路径
   * @param {*} moduleName 模块名称
   */
  loadServices(modules, moduleName) {
    this.loadFiles(modules, moduleName, "service");
  }

  /**
   * @description 扫描模块下所有controller
   * @param {*} moduleDir 模块路径
   * @param {*} moduleName 模块名称
   */
  loadControllers(modules, moduleName) {
    this.loadFiles(modules, moduleName, "controller");
  }

  /**
   * @description 扫描模块下所有router.js
   * @param {*} moduleDir 模块路径
   * @param {*} moduleName 模块名称
   */
  loadRoutes(modules, moduleName) {
    const routersDir = path.join(
      Chan.config.APP_PATH,
      modules,
      moduleName,
      "router.js"
    );
    if (fs.existsSync(routersDir)) {
      const routes = require(routersDir);
      routes({ router: this.router, modules: Chan[modules], app: this.app });
    }
  }

  //通用路由，加载错误处理和500路由和爬虫处理
  loadCommonRouter() {
    try {
      const baseRouterPath = path.join(Chan.config.APP_PATH, "router.js");
      if (fs.existsSync(baseRouterPath)) {
        const _router = require(baseRouterPath);
        _router(this.app, this.router, Chan.config);
      }
    } catch (error) {
      console.log(error);
    }
  }

  run(cb) {
    const port = Chan.config.port || "81";
    this.app.listen(port, () => {
      cb?cb(port):console.log(`Server is running on port ${port}`);
    });
  }
}
global.Chan = Chan;
module.exports = Chan;
