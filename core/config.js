const path = require('path');

/**
 * @description 根目录
 */
const ROOT_PATH = process.cwd();
// 读取package.json文件
const {version='1.0.0',author='明空'} = require(path.join(ROOT_PATH, 'package.json'));

/**
 * @description 程序目录
 */
const APP_PATH = path.join(ROOT_PATH, 'app');

let config = {
  JSON_LIMIT:"100kb",
  logger : {
    level: 'dev',
  },
  version,
  author,
  env:'dev',
  template:'default',
  views:[], //模板路径
  static:[{  //静态文件路径
    prefix: "/public/",
    dir: ["app/public"],
    maxAge: 0,
  }],
  database:{
    client: "mysql2",
    host: "localhost",
    port: "3306",
    user: "root",
    password: "123456",
    database: "chancms",
    charset: "utf8mb4",
  }
}

module.exports = {
  ROOT_PATH,
  APP_PATH, 
  ...config
}