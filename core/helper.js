const knex = require('knex');

/**
 * @description 实例化一个类，并将该类的所有方法绑定到一个新的对象上。
 * @param {Function} className - 需要实例化的类。
 *@returns {Object} 包含绑定方法的对象。
 */
exports.bindClass = function(className) {
  let obj = {};
  const cls = new className();
  Object.getOwnPropertyNames(cls.constructor.prototype).forEach(
    (methodName) => {
      if (
        methodName !== "constructor" &&
        typeof cls[methodName] === "function"
      ) {
        obj[methodName] = cls[methodName].bind(cls);
      }
    }
  );
  return obj;
}


exports.createKnex = function(opt) {
    let config = {
        host:"localhost",
        port:"3306",
        client:"mysql2",
        charset:"utf8mb4",
        debug:false,
        ...opt
    };
    return knex({
      client: config.client,
      connection: {
        host:config.host,
        port:config.port,
        user:config.user,
        password:config.password,
        database:config.database,
        charset:config.charset,
      },
      debug: config.debug,
      //默认为{min: 2, max: 10}
      pool: { 
        min: 0,
        max: 2,
      },
      log: {
        warn(message) {
          console.error("[knex warn]", message);
        },
        error(message) {
          console.error("[knex error]", message);
        },
      },
    });
}
/**
 * 
 * @param {*} module 模块目录
 * @returns Array 
 * @description 将web模块放到最后加载
 */
exports.loadWebToEnd = function(module=[]){
  const index = module.indexOf('web');
  if (index !== -1) {
      const web = module.splice(index, 1);
      module.push(web[0]);
  }
  return module;
}
