const express = require("express");
module.exports = (app)=>{
    app.plus = {
        setStatic:function ({prefix,dir,maxAge}) {
            app.use(prefix, express.static(dir, { maxAge: maxAge || 0 }));
        }
    }
}