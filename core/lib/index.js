const express = require("express");
const cookieParser = require("cookie-parser");
const favicon = require("serve-favicon");
const morgan = require("morgan");
const path = require("path");
const view = require("./view.js");
module.exports = async function (app, config) {
  const { logger, APP_PATH, cookieKey, static, JSON_LIMIT, appName, version } =
    config;

  app.use(morgan(logger.level));
  app.use(favicon(path.join(APP_PATH, "public/favicon.ico")));
  app.use(cookieParser(cookieKey));
  app.use(express.json({ limit: JSON_LIMIT }));
  app.use(express.urlencoded({ extended: false }));
  view(app, config);
  if (static.length > 0) {
    static.forEach((item) => {
      const { prefix, dir, maxAge } = item;
      app.use(prefix, express.static(dir, { maxAge: maxAge || 0 }));
    });
  }

  app.use((req, res, next) => {
    res.setHeader("Create-By", "Chanjs");
    res.setHeader("X-Powered-By", "ChanCMS");
    res.setHeader("ChanCMS", version);
    res.setHeader("Server", appName);
    next();
  });
};
