class Cache {
    constructor(maxSize = 1000) {
      this.cache = new Map(); // 使用 Map 存储缓存数据
      this.maxSize = maxSize; // 缓存最大容量
      this.hits = 0; // 缓存命中次数
      this.misses = 0; // 缓存未命中次数
    }
  
    /**
     * 设置缓存
     * @param {string} key - 缓存键
     * @param {any} value - 缓存值
     * @param {number} ttl - 缓存时间（毫秒），可选
     */
    set(key, value, ttl = null) {
      // 如果缓存已满，删除最早的一个缓存项
      if (this.cache.size >= this.maxSize) {
        const firstKey = this.cache.keys().next().value;
        this.cache.delete(firstKey);
      }
  
      const cacheItem = {
        value,
        expires: ttl ? Date.now() + ttl : null, // 设置过期时间
        updatedAt: Date.now(), // 记录更新时间
      };
      this.cache.set(key, cacheItem);
    }
  
    /**
     * 获取缓存
     * @param {string} key - 缓存键
     * @returns {any} - 缓存值，如果过期或不存在则返回 null
     */
    get(key) {
      if (!this.cache.has(key)) {
        this.misses++; // 未命中
        return null;
      }
  
      const cacheItem = this.cache.get(key);
  
      // 检查是否过期
      if (cacheItem.expires && Date.now() > cacheItem.expires) {
        this.cache.delete(key); // 删除过期缓存
        this.misses++; // 未命中
        return null;
      }
  
      this.hits++; // 命中
      return cacheItem.value; // 返回缓存值
    }
  
    /**
     * 清除指定缓存
     * @param {string} key - 缓存键
     */
    clear(key) {
      this.cache.delete(key);
    }
  
    /**
     * 清除所有缓存
     */
    clearAll() {
      this.cache.clear();
      this.hits = 0;
      this.misses = 0;
    }
  
    /**
     * 获取缓存命中率
     * @returns {number} - 命中率（0 到 1 之间）
     */
    getHitRate() {
      const total = this.hits + this.misses;
      return total === 0 ? 0 : this.hits / total;
    }
  
    /**
     * 获取缓存大小
     * @returns {number} - 缓存项数量
     */
    size() {
      return this.cache.size;
    }
  
    /**
     * 获取所有缓存键
     * @returns {string[]} - 缓存键列表
     */
    keys() {
      return Array.from(this.cache.keys());
    }
  
    /**
     * 遍历缓存项
     * @param {function} callback - 回调函数，接收 key 和 value 作为参数
     */
    forEach(callback) {
      this.cache.forEach((cacheItem, key) => {
        callback(key, cacheItem.value);
      });
    }
}

module.exports = new Cache();
  
//   // 示例用法
//   const cache = new SimpleCache(100); // 设置缓存最大容量为 100
  
//   // 设置缓存，有效期为 5 秒
//   cache.set('name', 'Alice', 5000);
//   cache.set('name', 'Alice', 5000);
//   // 获取缓存
//   console.log(cache.get('name')); // 输出: Alice
  
//   // 获取缓存命中率
//   console.log(cache.getHitRate()); // 输出: 1 (100% 命中率)
  
//   // 获取缓存大小
//   console.log(cache.size()); // 输出: 1
  
//   // 获取所有缓存键
//   console.log(cache.keys()); // 输出: ['name']
  
//   // 遍历缓存项
//   cache.forEach((key, value) => {
//     console.log(`Key: ${key}, Value: ${value}`);
//   });
  
//   // 清除所有缓存
//   cache.clearAll();
//   console.log(cache.size()); // 输出: 0