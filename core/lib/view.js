const path = require("path");
const template = require("art-template");
const dayjs = require("dayjs");

// 注册 dateFormat 函数
template.defaults.imports.dateFormat = function (date, format) {
  if (!date) {
    return "";
  }
  // 如果传入的是一个 Date 对象，转换为 dayjs 对象
  if (
    date instanceof Date ||
    typeof date === "string" ||
    typeof date === "number"
  ) {
    date = dayjs(date);
  } else {
    return "";
  }
  return date.format(format);
};

module.exports = (app, config) => {
  const { APP_PATH, views, env } = config;
 //合并插件中的view
  const all = [...views, 'app/modules/web/view'];

  app.set("view options", {
    debug: env === "dev",
    cache: env === "prd",
    minimize: true,
  });
  app.set("view engine", "html");
  app.set("views", all);
  app.engine(".html", require("express-art-template"));
};
