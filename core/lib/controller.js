/**
 * @description
 * 控制器基类
 */
class Controller {

  status = {
    200:"操作成功",
    201:"操作失败",
    //错误状态码
    401:"未授权",
    403:"禁止访问",
    404:"找不到资源",
    500:"服务器内部错误",
    //业务状态码
    1001:"参数错误",
    1002:"用户名或密码错误",
    1003:"用户名已存在",
    1004:"邮箱已存在",
    1005:"手机号已存在",
    1006:"验证码错误",
    1007:"验证码已过期",
    1008:"验证码发送失败",
    1009:"验证码发送成功",
    1010:"用户未登录",
    1011:"用户未注册",
    1012:"用户已注册",
    //资源验证码
    1013:"上传文件超过限制",
    1014:"操作不允许",
    1015:"资源不存在",
    //其他状态码
    9999:"系统内部错误",
    
 };

  constructor() {
   
  }

  // 获取状态
  getStatus() {
    return this.status
  }

  setTatus(key, val) {
    this.status[key] = val
  }

  success(res,data = {}) {
    res.json({
      code: 200,
      msg: this.status[200],
      data
    })
  }

  // 定义一个fail函数，用于返回错误信息
  fail(res,code,data, msg='操作失败') {
      res.json({
        code: code,
        msg: this.status[code] || msg,
        data
      })
  }
}

module.exports = Controller;
